//
// Created by root on 19.09.2021.
//

#include "tlscore.h"

gint tls_global_init(GError **error) {
    gint ret = gnutls_global_init();

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}
