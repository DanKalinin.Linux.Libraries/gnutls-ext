//
// Created by root on 20.01.2020.
//

#include "tlserror.h"

G_DEFINE_QUARK(tls-error-quark, tls_error)

void tls_set_error(GError **self, gint code) {
    gchar *message = (gchar *)gnutls_strerror(code);
    g_set_error_literal(self, TLS_ERROR, code, message);
}
