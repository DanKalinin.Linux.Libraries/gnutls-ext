//
// Created by root on 19.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSCORE_H
#define LIBRARY_GNUTLS_EXT_TLSCORE_H

#include "tlsmain.h"
#include "tlserror.h"

G_BEGIN_DECLS

gint tls_global_init(GError **error);

G_END_DECLS

#endif //LIBRARY_GNUTLS_EXT_TLSCORE_H
