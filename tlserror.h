//
// Created by root on 20.01.2020.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSERROR_H
#define LIBRARY_GNUTLS_EXT_TLSERROR_H

#include "tlsmain.h"

G_BEGIN_DECLS

#define TLS_ERROR tls_error_quark()

GQuark tls_error_quark(void);

void tls_set_error(GError **self, gint code);

G_END_DECLS

#endif //LIBRARY_GNUTLS_EXT_TLSERROR_H
