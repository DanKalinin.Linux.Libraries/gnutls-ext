//
// Created by root on 12.09.2021.
//

#include "tlsabstract.h"










gint tls_privkey_init(gnutls_privkey_t *self, GError **error) {
    gint ret = gnutls_privkey_init(self);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_privkey_import_x509(gnutls_privkey_t self, gnutls_x509_privkey_t key, guint flags, GError **error) {
    gint ret = gnutls_privkey_import_x509(self, key, flags);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_privkey_decrypt_data(gnutls_privkey_t self, guint flags, gnutls_datum_t *ciphertext, gnutls_datum_t *plaintext, GError **error) {
    gint ret = gnutls_privkey_decrypt_data(self, flags, ciphertext, plaintext);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}










gint tls_pubkey_init(gnutls_pubkey_t *self, GError **error) {
    gint ret = gnutls_pubkey_init(self);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_pubkey_import_privkey(gnutls_pubkey_t self, gnutls_privkey_t key, guint usage, guint flags, GError **error) {
    gint ret = gnutls_pubkey_import_privkey(self, key, usage, flags);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_pubkey_export_rsa_raw2(gnutls_pubkey_t self, gnutls_datum_t *m, gnutls_datum_t *e, guint flags, GError **error) {
    gint ret = gnutls_pubkey_export_rsa_raw2(self, m, e, flags);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}
