//
// Created by root on 12.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSABSTRACT_H
#define LIBRARY_GNUTLS_EXT_TLSABSTRACT_H

#include "tlsmain.h"
#include "tlserror.h"










G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_FREE_FUNC(gnutls_privkey_t, gnutls_privkey_deinit, NULL)

gint tls_privkey_init(gnutls_privkey_t *self, GError **error);
gint tls_privkey_import_x509(gnutls_privkey_t self, gnutls_x509_privkey_t key, guint flags, GError **error);
gint tls_privkey_decrypt_data(gnutls_privkey_t self, guint flags, gnutls_datum_t *ciphertext, gnutls_datum_t *plaintext, GError **error);

G_END_DECLS










G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_FREE_FUNC(gnutls_pubkey_t, gnutls_pubkey_deinit, NULL)

gint tls_pubkey_init(gnutls_pubkey_t *self, GError **error);
gint tls_pubkey_import_privkey(gnutls_pubkey_t self, gnutls_privkey_t key, guint usage, guint flags, GError **error);
gint tls_pubkey_export_rsa_raw2(gnutls_pubkey_t self, gnutls_datum_t *m, gnutls_datum_t *e, guint flags, GError **error);

G_END_DECLS










#endif //LIBRARY_GNUTLS_EXT_TLSABSTRACT_H
