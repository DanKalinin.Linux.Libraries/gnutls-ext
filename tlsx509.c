//
// Created by root on 12.09.2021.
//

#include "tlsx509.h"

gint tls_x509_privkey_init(gnutls_x509_privkey_t *self, GError **error) {
    gint ret = gnutls_x509_privkey_init(self);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_x509_privkey_import(gnutls_x509_privkey_t self, gnutls_datum_t *data, gnutls_x509_crt_fmt_t format, GError **error) {
    gint ret = gnutls_x509_privkey_import(self, data, format);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}
