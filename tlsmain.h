//
// Created by root on 19.01.2020.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSMAIN_H
#define LIBRARY_GNUTLS_EXT_TLSMAIN_H

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <glib-ext/glib-ext.h>

#endif //LIBRARY_GNUTLS_EXT_TLSMAIN_H
