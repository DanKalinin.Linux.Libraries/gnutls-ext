//
// Created by root on 13.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSCRYPTO_H
#define LIBRARY_GNUTLS_EXT_TLSCRYPTO_H

#include "tlsmain.h"
#include "tlserror.h"

G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_FREE_FUNC(gnutls_cipher_hd_t, gnutls_cipher_deinit, NULL)

gint tls_cipher_init(gnutls_cipher_hd_t *self, gnutls_cipher_algorithm_t cipher, gnutls_datum_t *key, gnutls_datum_t *iv, GError **error);
gint tls_cipher_encrypt2(gnutls_cipher_hd_t self, gpointer text, gsize textlen, gpointer ciphertext, gsize ciphertextlen, GError **error);

G_END_DECLS

#endif //LIBRARY_GNUTLS_EXT_TLSCRYPTO_H
