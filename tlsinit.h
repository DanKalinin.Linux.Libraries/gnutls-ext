//
// Created by root on 19.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSINIT_H
#define LIBRARY_GNUTLS_EXT_TLSINIT_H

#include "tlsmain.h"
#include "tlscore.h"

G_BEGIN_DECLS

void tls_init(void);

G_END_DECLS

#endif //LIBRARY_GNUTLS_EXT_TLSINIT_H
