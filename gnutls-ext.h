//
// Created by root on 19.01.2020.
//

#ifndef LIBRARY_GNUTLS_EXT_GNUTLS_EXT_H
#define LIBRARY_GNUTLS_EXT_GNUTLS_EXT_H

#include <gnutls-ext/tlsmain.h>
#include <gnutls-ext/tlsdatum.h>
#include <gnutls-ext/tlscore.h>
#include <gnutls-ext/tlsx509.h>
#include <gnutls-ext/tlsabstract.h>
#include <gnutls-ext/tlscrypto.h>
#include <gnutls-ext/tlsinit.h>

#endif //LIBRARY_GNUTLS_EXT_GNUTLS_EXT_H
