//
// Created by root on 12.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSX509_H
#define LIBRARY_GNUTLS_EXT_TLSX509_H

#include "tlsmain.h"
#include "tlserror.h"

G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_FREE_FUNC(gnutls_x509_privkey_t, gnutls_x509_privkey_deinit, NULL)

gint tls_x509_privkey_init(gnutls_x509_privkey_t *self, GError **error);
gint tls_x509_privkey_import(gnutls_x509_privkey_t self, gnutls_datum_t *data, gnutls_x509_crt_fmt_t format, GError **error);

G_END_DECLS

#endif //LIBRARY_GNUTLS_EXT_TLSX509_H
