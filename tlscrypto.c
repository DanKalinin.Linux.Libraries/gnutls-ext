//
// Created by root on 13.09.2021.
//

#include "tlscrypto.h"

gint tls_cipher_init(gnutls_cipher_hd_t *self, gnutls_cipher_algorithm_t cipher, gnutls_datum_t *key, gnutls_datum_t *iv, GError **error) {
    gint ret = gnutls_cipher_init(self, cipher, key, iv);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_cipher_encrypt2(gnutls_cipher_hd_t self, gpointer text, gsize textlen, gpointer ciphertext, gsize ciphertextlen, GError **error) {
    gint ret = gnutls_cipher_encrypt2(self, text, textlen, ciphertext, ciphertextlen);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}
