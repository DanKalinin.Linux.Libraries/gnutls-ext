//
// Created by root on 13.09.2021.
//

#include "tlsdatum.h"

gint tls_hex_encode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error) {
    gint ret = gnutls_hex_encode2(self, result);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_hex_decode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error) {
    gint ret = gnutls_hex_decode2(self, result);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_base64_encode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error) {
    gint ret = gnutls_base64_encode2(self, result);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

gint tls_base64_decode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error) {
    gint ret = gnutls_base64_decode2(self, result);

    if (ret < GNUTLS_E_SUCCESS) {
        tls_set_error(error, ret);
    }

    return ret;
}

void tls_datum_free(gnutls_datum_t *self) {
    gnutls_free(self->data);
    g_free(self);
}
