//
// Created by root on 13.09.2021.
//

#ifndef LIBRARY_GNUTLS_EXT_TLSDATUM_H
#define LIBRARY_GNUTLS_EXT_TLSDATUM_H

#include "tlsmain.h"
#include "tlserror.h"

G_BEGIN_DECLS

gint tls_hex_encode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error);
gint tls_hex_decode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error);
gint tls_base64_encode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error);
gint tls_base64_decode2(gnutls_datum_t *self, gnutls_datum_t *result, GError **error);
void tls_datum_free(gnutls_datum_t *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(gnutls_datum_t, tls_datum_free)

#endif //LIBRARY_GNUTLS_EXT_TLSDATUM_H
